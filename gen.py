from lxml import etree as ET
import sys
from math import sin, cos, pi
from itertools import islice
svg = ET.Element('svg', attrib=dict(
   xmlns="http://www.w3.org/2000/svg",
   version="1.1",
   viewBox="0 0 2560 1280"
))
GRID = 320
WIDTH = 2560 // GRID
MARGIN = 40
RADIUS = (GRID-MARGIN) / 2
g = ET.Element('g')
svg.append(g)

def fib(a, b):
    yield a
    yield from fib(b, a+b)

def curves(num, radius, ratio):
    out = ''
    for i in range(0, num):
        x = i * 2 * pi / num 
        out += '{},{} {},{}, {},{}\n'.format(
            (radius/ratio)*sin(x), (radius/ratio)*cos(x),
            radius*cos(x)+(radius/ratio)*sin(x), (radius/ratio)*cos(x)-radius*sin(x),
            radius*cos(x), -radius*sin(x)
        )
        out += '{},{} {},{}, {},{}\n'.format(
            -(radius/ratio)*sin(x), -(radius/ratio)*cos(x),
            -radius*cos(x)-(radius/ratio)*sin(x), -(radius/ratio)*cos(x)+radius*sin(x),
            -radius*cos(x), +radius*sin(x)
        )
    return out

if sys.argv[1] == '1':
    for j, ratio in enumerate([8,4,2,1]):
        for i, n in enumerate(islice(fib(2,3), 0, WIDTH)):
            x = GRID/2 + GRID*i; y = GRID/2 + GRID * j
            g.append(ET.Element('path', attrib=dict(
                style='fill:none;stroke:#000000;stroke-width:5px',
                d='m {},{} c'.format(x,y)+curves(n, radius=RADIUS, ratio=ratio),
            )))

elif sys.argv[1] == '2':
    for i, n in enumerate(islice(fib(2,3), 0, WIDTH)):
        x = GRID/2 + GRID*i; y = GRID/2 + GRID * 0
        g.append(ET.Element('path', attrib=dict(
            style='fill:none;stroke:#000000;stroke-width:{}px'.format((WIDTH-i)*(5/WIDTH)),
            d='m {},{} c'.format(x,y)+curves(n, radius=RADIUS*0.7, ratio=0.5),
        )))

    for j, ratio in enumerate([8,4,2,1]):
        for i, n in enumerate(islice(fib(2,3), 0, WIDTH)):
            x = GRID/2 + GRID*i; y = GRID/2 + GRID * 1
            g.append(ET.Element('path', attrib=dict(
                style='fill:none;stroke:#000000;stroke-width:{}px'.format((WIDTH-i)*(5/WIDTH)),
                d='m {},{} c'.format(x,y)+curves(n, radius=RADIUS, ratio=ratio),
            )))

    for i, n in enumerate(islice(fib(2,3), 0, WIDTH)):
        x = GRID/2 + GRID*i; y = GRID/2 + GRID * 2
        g.append(ET.Element('path', attrib=dict(
            style='fill:none;stroke:#000000;stroke-width:{}px'.format((WIDTH-i)*(5/WIDTH)),
            d='m {},{} c'.format(x,y)+curves(n, radius=RADIUS*0.8, ratio=1),
        )))

    for i, n in enumerate(islice(fib(2,3), 0, WIDTH)):
        x = GRID/2 + GRID*i; y = GRID/2 + GRID * 2
        g.append(ET.Element('path', attrib=dict(
            style='fill:none;stroke:#000000;stroke-width:{}px'.format((WIDTH-i)*(5/WIDTH)),
            d='m {},{} c'.format(x,y)+curves(n, radius=RADIUS, ratio=16),
        )))

    for i, n in enumerate(islice(fib(2,3), 0, WIDTH)):
        x = GRID/2 + GRID*i; y = GRID/2 + GRID * 3
        g.append(ET.Element('path', attrib=dict(
            style='fill:none;stroke:#000000;stroke-width:{}px'.format((WIDTH-i)*(5/WIDTH)),
            d='m {},{} c'.format(x,y)+curves(n, radius=RADIUS*0.7, ratio=0.5),
        )))

    for i, n in enumerate(islice(fib(2,3), 0, WIDTH)):
        x = GRID/2 + GRID*i; y = GRID/2 + GRID * 3
        g.append(ET.Element('path', attrib=dict(
            style='fill:none;stroke:#000000;stroke-width:{}px'.format((WIDTH-i)*(5/WIDTH)),
            d='m {},{} c'.format(x,y)+curves(n, radius=RADIUS, ratio=16),
        )))


sys.stdout.buffer.write(ET.tostring(svg))
